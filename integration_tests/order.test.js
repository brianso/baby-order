const supertest = require('supertest');

const request = supertest('http://127.0.0.1:8080');
// const request = supertest('https://babyorder.brianso.space');

const STATUS_UNASSIGNED = "UNASSIGNED";
const STATUS_TAKEN = "TAKEN";
const STATUS_SUCCESS = "SUCCESS";

describe('api', () => {
   describe('POST /orders', () => {
       describe('valid requests', () => {
           test('args = two pts in tai po', async () => {
               const resp = await request
                   .post('/orders')
                   .send({
                       origin: ["22.4496768", "114.1624143"],
                       destination: ["22.4499398", "114.1628275"],
                   })
                   .expect(200);
               expect(resp.body.distance).toBe(52);
               expect(resp.body.status).toEqual(STATUS_UNASSIGNED);
           });

           test('args = two pts far away', async () => {
               const resp = await request
                   .post('/orders')
                   .send({
                       origin: ["22.4496768", "114.1624143"],
                       destination: ["25.0175849", "121.2254957"],
                   })
                   .expect(409);
               expect(typeof resp.body.error).toBe("string");
               expect(resp.body.error).toBe("Distance cannot be calculated for the requested origin and destination");
           });
       });

       describe('invalid requests', () => {
           test('args = non number', async () => {
               const resp = await request
                   .post('/orders')
                   .send({
                       origin: ["a", "b"],
                       destination: ["a", "b"],
                   })
                   .expect(400);
               expect(typeof resp.body.error).toBe("string");
               expect(resp.body.error).toBeTruthy();
           });
       });

   });

   describe("PATCH /orders/:id", () => {
       describe("valid request", () => {

           let orderId = 0;

           beforeEach(async () => {
              const resp = await request.post('/orders').send({
                  origin: ["22.4496768", "114.1624143"],
                  destination: ["22.4499398", "114.1628275"],
              }).expect(200);
              orderId = resp.body.id;
           });

           test("take a new order", async () => {
               const resp = await request.patch(`/orders/${orderId}`)
                   .send({
                       status: STATUS_TAKEN
                   }).expect(200);
               expect(resp.body.status).toBe(STATUS_SUCCESS);
           });

           test("race condition", async () => {
               let [resp1, resp2] = await Promise.all([
                   request.patch(`/orders/${orderId}`)
                       .send({
                           status: STATUS_TAKEN
                       }),
                   request.patch(`/orders/${orderId}`)
                       .send({
                           status: STATUS_TAKEN
                       })
               ]);

               if (resp2.status === 200) {
                   [resp1, resp2] = [resp2, resp1]
               }
               expect(resp1.status).toEqual(200);
               expect(resp2.status).toEqual(409);
               expect(resp1.body.status).toEqual(STATUS_SUCCESS);
               expect(resp2.body.error).toBeTruthy();
           });
       });

       describe("invalid request",  () => {
           test("invalid orderid", async () => {
               await request.patch(`/orders/abc`)
                   .send({
                       status: STATUS_TAKEN
                   }).expect(400)
           });

           test("invalid status", async () => {
               await request.patch(`/orders/1`)
                   .send({
                       status: "abc"
                   }).expect(400)
           });
       });
   });

   describe("GET /orders", () => {

       beforeAll(async () => {
           for (let i = 0; i < 5; i ++) {
               await request.post('/orders').send({
                   origin: ["22.4496768", "114.1624143"],
                   destination: ["22.4499398", "114.1628275"],
               }).expect(200);
           }
       });

       describe("valid requests", () => {
           test("page 1 limit 3", async () => {
               const resp = await request.get('/orders?page=1&limit=3').expect(200);
               expect(resp.body.length).toBe(3);
           });
           test("page 1 limit 1", async () => {
               const resp = await request.get('/orders?page=1&limit=1').expect(200);
               expect(resp.body.length).toBe(1);
           });
           test("page 1000 limit 1000", async () => {
               const resp = await request.get('/orders?page=1000&limit=1000').expect(200);
               expect(resp.body.length).toBe(0);
           });
           test("page 1 limit 10000", async () => {
               const resp1 = await request.get('/orders?page=1&limit=1000').send().expect(200);

               // place an order
               const placeResp = await request.post('/orders').send({
                   origin: ["22.4496768", "114.1624143"],
                   destination: ["22.4499398", "114.1628275"],
               }).expect(200);

               const resp2 = await request.get('/orders?page=1&limit=1000').send().expect(200);

               expect(resp2.body.length).toBe(resp1.body.length + 1);
               expect(resp2.body[resp2.body.length - 1].status).toBe(STATUS_UNASSIGNED);

               // take an order
               await request.patch(`/orders/${placeResp.body.id}`).send({
                   status: STATUS_TAKEN,
               }).expect(200);

               const resp3 = await request.get('/orders?page=1&limit=1000').send().expect(200);

               expect(resp3.body.length).toBe(resp1.body.length + 1);
               expect(resp3.body[resp3.body.length - 1].status).toBe(STATUS_TAKEN);
           });
       });
       describe("invalid requests", () => {
           test("invalid page 0", async () => {
               const resp = await request.get('/orders?page=0&limit=1000').send().expect(400);
               expect(resp.body.error).toBeTruthy()
           });

           test("invalid page ''", async () => {
               const resp = await request.get('/orders?page=&limit=1000').send().expect(400);
               expect(resp.body.error).toBeTruthy()
           });

           test("invalid page 1.1", async () => {
               const resp = await request.get('/orders?page=1.1&limit=1000').send().expect(400);
               expect(resp.body.error).toBeTruthy()
           });

           test("invalid limit 0", async () => {
               const resp = await request.get('/orders?page=1&limit=1.1').send().expect(400);
               expect(resp.body.error).toBeTruthy()
           });

           test("invalid limit 1.1", async () => {
               const resp = await request.get('/orders?page=1&limit=1.1').send().expect(400);
               expect(resp.body.error).toBeTruthy()
           });
       });
   })
});