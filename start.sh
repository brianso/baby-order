#!/bin/bash

# Read env variable from .env
export $(egrep -v '^#' .env.example | xargs)
export $(egrep -v '^#' .env | xargs)

docker-compose build
docker-compose up migrations
docker-compose up $@ app