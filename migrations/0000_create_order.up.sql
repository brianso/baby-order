CREATE TABLE `orders` (
    `id` BIGINT(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
    `origin_lat` DOUBLE NOT NULL,
    `origin_lng` DOUBLE NOT NULL,
    `destination_lat` DOUBLE NOT NULL,
    `destination_lng` DOUBLE NOT NULL,
    `status` VARCHAR(20) NOT NULL,
    `distance` INT UNSIGNED NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
