package main

import (
	"baby-order/pkg/server"

	log "github.com/sirupsen/logrus"
)

func main() {
	srv, err := server.NewServer()
	if err != nil {
		log.Fatalf("%+v\n", err)
	}

	err = srv.Run("0.0.0.0:8080")
	if err != nil {
		log.Fatalf("%+v\n", err)
	}
}
