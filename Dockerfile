# build stage
FROM golang:1.13.0 as builder
ENV GO111MODULE=on

WORKDIR /app

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY entrypoints ./entrypoints
COPY pkg ./pkg

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build ./entrypoints/order

# final stage
FROM alpine:latest AS final
COPY --from=builder /app/order /app

HEALTHCHECK --interval=10s --timeout=3s \
  CMD wget --quiet --tries=1 --spider http://localhost:8080/ping || exit 1

EXPOSE 8080
ENTRYPOINT ["/app"]
