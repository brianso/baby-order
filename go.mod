module baby-order

go 1.13

require (
	github.com/DATA-DOG/go-sqlmock v1.3.3
	github.com/gin-gonic/gin v1.4.0
	github.com/google/uuid v1.1.1 // indirect
	github.com/jinzhu/gorm v1.9.10
	github.com/kr/pretty v0.1.0 // indirect
	github.com/pkg/errors v0.8.1
	github.com/sergi/go-diff v1.0.0 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.3.0
	googlemaps.github.io/maps v0.0.0-20190909213747-3c037358a0f0
)
