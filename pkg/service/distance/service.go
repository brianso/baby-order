package distance

import "baby-order/pkg/common/types"

type Service interface {
	// Get the driving distance between two points
	//  return errors.ErrNoDistance if the driving route cannot be determined
	GetDrivingDistance(coord1 types.Coordinate, coord2 types.Coordinate) (uint, error)
}
