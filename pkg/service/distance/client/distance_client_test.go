package client

import (
	babyerror "baby-order/pkg/common/errors"
	"baby-order/pkg/common/types"
	"testing"

	"github.com/pkg/errors"

	"github.com/stretchr/testify/require"
)

func TestDistanceClient_GetDistance(t *testing.T) {
	client, err := NewDistanceClient()
	require.NoError(t, err, "NewDistanceClient error")

	t.Run("Normal Request: two points in Tai Po", func(t *testing.T) {
		// test data source
		// https://www.google.com.hk/maps/dir/22.4496768,114.1624143/22.4499398,114.1628275/@22.4497852,114.1625435,21z/data=!4m2!4m1!3e2?hl=zh-TW
		dist, err := client.GetDrivingDistance(types.Coordinate{
			Lat: 22.4496768,
			Lng: 114.1624143,
		}, types.Coordinate{
			Lat: 22.4499398,
			Lng: 114.1628275,
		})
		require.NoError(t, err, "GetDrivingDistance error")
		require.Equal(t, uint(52), dist)
	})

	t.Run("Long distance Request: Hong Kong and Taiwan", func(t *testing.T) {
		_, err := client.GetDrivingDistance(types.Coordinate{
			Lat: 22.4496768,
			Lng: 114.1624143,
		}, types.Coordinate{
			Lat: 25.0175849,
			Lng: 121.2254957,
		})
		require.Error(t, err, "Should return an error")
		require.Equal(t, errors.Cause(err), babyerror.ErrNoDistance)
	})
}
