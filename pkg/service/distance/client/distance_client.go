package client

import (
	"baby-order/pkg/common/config"
	"baby-order/pkg/common/errors"
	"baby-order/pkg/common/types"
	"baby-order/pkg/service/distance"
	"context"
	"fmt"
	"strconv"

	"googlemaps.github.io/maps"
)

// Implements distance.Service
// It requests to the google map api service
type distanceClient struct {
	client *maps.Client
}

const (
	statusOK = "OK"
)

func NewDistanceClient() (distance.Service, error) {
	client, err := maps.NewClient(maps.WithAPIKey(config.GetGoogleMapsApiKey()))
	if err != nil {
		return nil, errors.Wrap(err, "NewDistanceClient error")
	}
	return &distanceClient{
		client: client,
	}, nil
}

// Request to google maps distance matrix API
// https://developers.google.com/maps/documentation/distance-matrix/intro
func (c *distanceClient) GetDrivingDistance(coord1 types.Coordinate, coord2 types.Coordinate) (uint, error) {
	rsp, err := c.client.DistanceMatrix(context.Background(), &maps.DistanceMatrixRequest{
		Origins:      []string{toGoogleMapCoordinate(coord1)},
		Destinations: []string{toGoogleMapCoordinate(coord2)},
	})
	if err != nil {
		return 0, errors.Wrap(err, "GetDrivingDistance error")
	}
	if len(rsp.Rows) == 0 || len(rsp.Rows[0].Elements) == 0 {
		return 0, errors.ErrUnexpectedRemoteResponse
	}

	// there should be exactly 1 row and 1 element in the response
	ele := rsp.Rows[0].Elements[0]
	if ele.Status != statusOK {
		return 0, errors.ErrNoDistance.WithStack()
	}
	rspDistance := rsp.Rows[0].Elements[0].Distance
	return uint(rspDistance.Meters), nil
}

// google map request format example: "41.43206,-81.38992"
func toGoogleMapCoordinate(coord types.Coordinate) string {
	return fmt.Sprintf(
		"%s,%s",
		strconv.FormatFloat(coord.Lat, 'f', 7, 64),
		strconv.FormatFloat(coord.Lng, 'f', 7, 64),
	)
}
