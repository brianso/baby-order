package impl

import (
	"baby-order/pkg/common/errors"
	"baby-order/pkg/common/types"
	"baby-order/pkg/db"
	"baby-order/pkg/db/model"
	"baby-order/pkg/service/distance"
	"baby-order/pkg/service/order"
)

type orderService struct {
	distSrv distance.Service
}

func NewService(distSrv distance.Service) order.Service {
	return &orderService{
		distSrv: distSrv,
	}
}

func (s *orderService) PlaceOrder(origin types.Coordinate, destination types.Coordinate) (*model.Order, error) {
	rspDistance, err := s.distSrv.GetDrivingDistance(origin, destination)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	result := model.Order{
		OriginLat:      origin.Lat,
		OriginLng:      origin.Lng,
		DestinationLat: destination.Lat,
		DestinationLng: destination.Lng,
		Status:         model.OrderStatusUnassigned,
		Distance:       rspDistance,
	}
	err = db.DB().Create(&result).Error
	if err != nil {
		return nil, errors.Wrap(err, "Place Order create error")
	}
	return &result, nil
}

func (*orderService) ModifyOrderStatus(orderId uint64, newStatus string) (*model.Order, error) {
	switch newStatus {
	case model.OrderStatusTaken:
		query := db.DB().Model(&model.Order{}).
			Where("id = ? AND status = ?", orderId, model.OrderStatusUnassigned).
			Updates(model.Order{
				Status: model.OrderStatusTaken,
			})
		if query.Error != nil {
			return nil, errors.Wrap(query.Error, "ModifyOrder Update error")
		}
		if query.RowsAffected == 0 {
			return nil, errors.ErrUnassignedOrderNotFound.WithStack()
		}

		var r model.Order
		err := db.DB().Where("id = ?", orderId).First(&r).Error
		if err != nil {
			return nil, errors.Wrap(err, "Query Order error")
		}

		return &r, err
	default:
		return nil, errors.ErrUnsupportedOrderStatus.WithStack()
	}
}

func (*orderService) GetOrderList(pageQuery types.PageQuery) ([]*model.Order, error) {
	var orders []*model.Order

	err := db.PageDB(pageQuery).Find(&orders).Error
	if err != nil {
		return nil, errors.Wrap(err, "GetOrderList query error")
	}

	return orders, nil
}
