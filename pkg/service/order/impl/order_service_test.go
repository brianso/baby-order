package impl

import (
	"baby-order/pkg/common/errors"
	"baby-order/pkg/common/types"
	"baby-order/pkg/db"
	"baby-order/pkg/db/model"
	"baby-order/pkg/service/distance/mocks"
	"baby-order/pkg/service/order"
	"database/sql"
	"database/sql/driver"
	"fmt"
	"regexp"
	"testing"

	"github.com/stretchr/testify/suite"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/require"
)

type Suite struct {
	suite.Suite
	sqlMock sqlmock.Sqlmock

	service         order.Service
	distServiceMock *mocks.Service

	orderRows []string
}

func TestSuite(t *testing.T) {
	suite.Run(t, new(Suite))
}

func (s *Suite) SetupSuite() {
	var (
		sqlDB *sql.DB
		err   error
	)

	sqlDB, s.sqlMock, err = sqlmock.New()
	require.NoError(s.T(), err)

	gormDB, err := gorm.Open("mysql", sqlDB)
	db.InjectMockDB(gormDB)
	require.NoError(s.T(), err)

	gormDB.LogMode(true)

	s.distServiceMock = &mocks.Service{}
	s.service = NewService(s.distServiceMock)

	s.orderRows = []string{
		"id",
		"originLat",
		"originLng",
		"destinationLat",
		"destinationLng",
		"status",
		"distance",
	}
}

func (s *Suite) TestOrderService_PlaceOrder() {

	testcases := map[string]struct {
		origin         types.Coordinate
		dest           types.Coordinate
		returnDist     uint
		expectSqlArgs  []driver.Value
		sqlReturn      sql.Result
		expectID       uint64
		expectDistance uint
	}{
		"0 values": {
			origin: types.Coordinate{
				Lat: 0,
				Lng: 0,
			},
			dest: types.Coordinate{
				Lat: 0,
				Lng: 0,
			},
			returnDist:     0,
			expectSqlArgs:  []driver.Value{float64(0), float64(0), float64(0), float64(0), model.OrderStatusUnassigned, uint(0)},
			sqlReturn:      sqlmock.NewResult(1, 1),
			expectID:       1,
			expectDistance: 0,
		},
		"two points": {
			origin: types.Coordinate{
				Lat: 22.7716293,
				Lng: 110.7299303,
			},
			dest: types.Coordinate{
				Lat: 12.7716293,
				Lng: 121.7249303,
			},
			returnDist:     123,
			expectSqlArgs:  []driver.Value{22.7716293, 110.7299303, 12.7716293, 121.7249303, model.OrderStatusUnassigned, uint(123)},
			sqlReturn:      sqlmock.NewResult(1, 1),
			expectID:       1,
			expectDistance: 123,
		},
	}

	for name, c := range testcases {
		s.Run(name, func() {
			s.SetupSuite()
			s.distServiceMock.
				On("GetDrivingDistance", c.origin, c.dest).
				Return(c.returnDist, nil)

			s.sqlMock.ExpectBegin()
			s.sqlMock.
				ExpectExec("INSERT  INTO `orders` \\(`origin_lat`,`origin_lng`,`destination_lat`,`destination_lng`,`status`,`distance`\\)").
				WithArgs(c.expectSqlArgs...).
				WillReturnResult(c.sqlReturn)
			s.sqlMock.ExpectCommit()

			respOrder, err := s.service.PlaceOrder(c.origin, c.dest)

			s.distServiceMock.AssertExpectations(s.T())

			require.NoError(s.T(), err)
			require.Equal(s.T(), respOrder.ID, c.expectID)
			require.Equal(s.T(), respOrder.Distance, c.expectDistance)
		})
	}

	s.Run("two points with errors", func() {
		s.SetupSuite()
		s.distServiceMock.
			On("GetDrivingDistance", types.Coordinate{
				Lat: 0,
				Lng: 0,
			}, types.Coordinate{
				Lat: 0,
				Lng: 0,
			}).
			Return(uint(0), errors.ErrNoDistance)

		_, err := s.service.PlaceOrder(types.Coordinate{
			Lat: 0,
			Lng: 0,
		}, types.Coordinate{
			Lat: 0,
			Lng: 0,
		})

		s.distServiceMock.AssertExpectations(s.T())

		require.Error(s.T(), err, "Should return an error")
	})
}

func (s *Suite) TestOrderService_ModifyOrderStatus() {
	s.Run("success case", func() {
		s.SetupSuite()
		orderID := uint64(1)

		s.sqlMock.ExpectBegin()
		s.sqlMock.ExpectExec("UPDATE  `orders` SET `status` = \\?").
			WithArgs(model.OrderStatusTaken, orderID, model.OrderStatusUnassigned).
			WillReturnResult(sqlmock.NewResult(0, 1))
		s.sqlMock.ExpectCommit()
		s.sqlMock.ExpectQuery(regexp.QuoteMeta("SELECT * FROM `orders` WHERE (id = ?)")).
			WithArgs(orderID).
			WillReturnRows(sqlmock.NewRows(s.orderRows).AddRow(
				"1",
				"11.1129401",
				"102.4444112",
				"11.4444433",
				"101.4444433",
				model.OrderStatusTaken,
				"100",
			))

		// Execute
		changedOrder, err := s.service.ModifyOrderStatus(orderID, model.OrderStatusTaken)

		require.NoError(s.T(), s.sqlMock.ExpectationsWereMet())

		require.NoError(s.T(), err)
		require.Equal(s.T(), changedOrder.ID, orderID)
		require.Equal(s.T(), changedOrder.Status, model.OrderStatusTaken)
	})
	s.Run("failed case", func() {
		s.SetupSuite()
		orderID := uint64(1)

		s.sqlMock.ExpectBegin()
		s.sqlMock.ExpectExec("UPDATE  `orders` SET `status` = \\?").
			WithArgs(model.OrderStatusTaken, orderID, model.OrderStatusUnassigned).
			WillReturnResult(sqlmock.NewResult(0, 0))
		s.sqlMock.ExpectCommit()

		// Execute
		_, err := s.service.ModifyOrderStatus(orderID, model.OrderStatusTaken)

		require.Error(s.T(), err)
	})
	s.Run("invalid status case", func() {
		s.SetupSuite()
		orderID := uint64(1)

		// Execute
		_, err := s.service.ModifyOrderStatus(orderID, model.OrderStatusUnassigned)

		require.Error(s.T(), err)
	})
}

func (s *Suite) TestOrderService_GetOrderList() {

	testcases := map[string]struct {
		page   uint
		limit  uint
		offset uint
	}{
		"page 1":          {page: 1, limit: 10, offset: 0},
		"page 10":         {page: 10, limit: 10, offset: 90},
		"page 10 limit 1": {page: 10, limit: 1, offset: 9},
	}

	for name, c := range testcases {
		s.Run(name, func() {
			s.SetupSuite()

			s.sqlMock.ExpectQuery(fmt.Sprintf(
				"SELECT \\* FROM `orders`.* LIMIT %v OFFSET %v", c.limit, c.offset,
			)).
				WillReturnRows(sqlmock.NewRows(s.orderRows).AddRow(
					"1",
					"11.1129401",
					"102.4444112",
					"11.4444433",
					"101.4444433",
					model.OrderStatusTaken,
					"100",
				).AddRow(
					"2",
					"11.1129401",
					"102.4444112",
					"11.4444433",
					"101.4444433",
					model.OrderStatusTaken,
					"100",
				))

			// Execute
			orders, err := s.service.GetOrderList(types.PageQuery{
				Page:  c.page,
				Limit: c.limit,
			})

			require.NoError(s.T(), s.sqlMock.ExpectationsWereMet())
			require.NoError(s.T(), err)

			require.Equal(s.T(), 2, len(orders))
			require.Equal(s.T(), uint64(2), orders[1].ID)
		})
	}

	s.Run("return empty array", func() {
		s.SetupSuite()

		s.sqlMock.ExpectQuery("SELECT \\* FROM `orders`").
			WillReturnRows(sqlmock.NewRows(s.orderRows))

		// Execute
		orders, err := s.service.GetOrderList(types.PageQuery{
			Page:  1,
			Limit: 1,
		})

		require.NoError(s.T(), s.sqlMock.ExpectationsWereMet())
		require.NoError(s.T(), err)

		require.Equal(s.T(), 0, len(orders))
	})

}
