package order

import (
	"baby-order/pkg/common/types"
	"baby-order/pkg/db/model"
)

type Service interface {

	// Place an order
	// place an order with the origin and destination to the service
	// returns an created order
	PlaceOrder(origin types.Coordinate, destination types.Coordinate) (*model.Order, error)

	// Modify an existing order's status
	// returns the modified order
	ModifyOrderStatus(orderId uint64, newStatus string) (*model.Order, error)

	// Get a list of created order
	GetOrderList(pageQuery types.PageQuery) ([]*model.Order, error)
}
