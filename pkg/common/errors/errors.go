// Project level error
package errors

import (
	"net/http"

	_ "github.com/pkg/errors"
)

// 400
var (
	ErrInvalidRequest          = NewBabyOrderError(http.StatusBadRequest, "Invalid request")
	ErrInvalidCoordinateFormat = NewBabyOrderError(http.StatusBadRequest, "Invalid request: coordinate must be an array of two float strings")
	ErrInvalidCoordinateValue  = NewBabyOrderError(http.StatusBadRequest, "Invalid request: lat must be within (-90, +90), lng must be within (-180, +180)")
	ErrUnsupportedOrderStatus  = NewBabyOrderError(http.StatusBadRequest, "Invalid request: status is not supported")
	ErrInvalidOrderID          = NewBabyOrderError(http.StatusBadRequest, "Invalid request: order id is invalid")
	ErrInvalidPageValue        = NewBabyOrderError(http.StatusBadRequest, "Invalid request: page must be start with 1")
)

// 409
var (
	ErrNoDistance              = NewBabyOrderError(http.StatusConflict, "Distance cannot be calculated for the requested origin and destination")
	ErrUnassignedOrderNotFound = NewBabyOrderError(http.StatusConflict, "Cannot find an unassigned order")
)

// 500
var (
	// stub error used when a func is not implemented
	ErrNotImplemented           = NewBabyOrderError(http.StatusInternalServerError, "Not Implemented")
	ErrUnexpectedRemoteResponse = NewBabyOrderError(http.StatusInternalServerError, "Expected Remote Response")
)
