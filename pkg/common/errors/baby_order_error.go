package errors

import (
	"fmt"

	"github.com/pkg/errors"
)

// The Base Error of this project baby-order
type BabyOrderError struct {
	Status  int
	Message string
}

func NewBabyOrderError(status int, message string) BabyOrderError {
	return BabyOrderError{
		Status:  status,
		Message: message,
	}
}

func (e BabyOrderError) Error() string {
	return fmt.Sprintf("(%d) %s", e.Status, e.Message)
}

// return an error with error stack information
// the stack can be viewed when formatted with %+v
func (e BabyOrderError) WithStack() error {
	return errors.WithStack(e)
}

// same as /github.com/pkg/errors
func WithStack(err error) error {
	return errors.WithStack(err)
}

// same as /github.com/pkg/errors
func Wrap(err error, message string) error {
	return errors.Wrap(err, message)
}
