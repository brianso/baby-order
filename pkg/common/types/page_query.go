package types

type PageQuery struct {
	Page  uint `form:"page" json:"page"`
	Limit uint `form:"limit" json:"limit"`
}
