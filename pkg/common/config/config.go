// Project level configuration

package config

import (
	"os"
)

type Config struct {
	DBName     string
	DBHost     string
	DBPort     string
	DBUser     string
	DBPassword string

	GoogleMapsApiKey string
}

var configStore Config

func init() {
	configStore = Config{
		DBHost:           os.Getenv("DB_HOST"),
		DBName:           os.Getenv("DB_NAME"),
		DBPort:           os.Getenv("DB_PORT"),
		DBUser:           os.Getenv("DB_USER"),
		DBPassword:       os.Getenv("DB_PASSWORD"),
		GoogleMapsApiKey: os.Getenv("GOOGLE_MAPS_API_KEY"),
	}
}

func GetDBHost() string {
	return configStore.DBHost
}

func GetDBPort() string {
	return configStore.DBPort
}

func GetDBUser() string {
	return configStore.DBUser
}

func GetDBPassword() string {
	return configStore.DBPassword
}

func GetDBName() string {
	return configStore.DBName
}

func GetGoogleMapsApiKey() string {
	return configStore.GoogleMapsApiKey
}
