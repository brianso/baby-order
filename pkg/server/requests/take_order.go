package requests

import (
	"baby-order/pkg/common/errors"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
)

type TakeOrderRequest struct {
	ID     string `uri:"id"`
	Status string `form:"status" json:"status"`
}

type TakeOrderRequestValidator struct {
	TakeOrderRequest
	ID uint64 `binding:"-"`
}

func (s *TakeOrderRequestValidator) Bind(c *gin.Context) error {
	b := binding.Default(c.Request.Method, c.ContentType())
	err := c.ShouldBindWith(s, b)
	if err != nil {
		return errors.ErrInvalidRequest.WithStack()
	}
	err = c.BindUri(s)
	if err != nil {
		return errors.ErrInvalidRequest.WithStack()
	}

	s.ID, err = strconv.ParseUint(s.TakeOrderRequest.ID, 10, 64)
	if err != nil {
		return errors.ErrInvalidOrderID.WithStack()
	}

	return nil
}
