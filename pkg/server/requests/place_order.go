package requests

import (
	"baby-order/pkg/common/errors"
	"baby-order/pkg/common/types"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
)

type PlaceOrderRequest struct {
	Origin      Coordinate `form:"origin" json:"origin" binding:"required"`
	Destination Coordinate `form:"destination" json:"destination" binding:"required"`
}

type PlaceOrderRequestValidator struct {
	PlaceOrderRequest
	Origin      types.Coordinate `binding:"-"`
	Destination types.Coordinate `binding:"-"`
}

func (s *PlaceOrderRequestValidator) Bind(c *gin.Context) error {
	b := binding.Default(c.Request.Method, c.ContentType())
	err := c.ShouldBindWith(s, b)
	if err != nil {
		return errors.ErrInvalidRequest.WithStack()
	}
	s.Origin, err = convertCoordinate(s.PlaceOrderRequest.Origin)
	if err != nil {
		return err
	}
	s.Destination, err = convertCoordinate(s.PlaceOrderRequest.Destination)
	if err != nil {
		return err
	}
	return nil
}

func convertCoordinate(coordinate Coordinate) (r types.Coordinate, err error) {
	if len(coordinate) != 2 {
		return r, errors.ErrInvalidCoordinateFormat.WithStack()
	}

	// convert to float64
	lat, err := strconv.ParseFloat(coordinate[0], 64)
	if err != nil {
		return r, errors.ErrInvalidCoordinateFormat.WithStack()
	}

	// convert to float64
	lng, err := strconv.ParseFloat(coordinate[1], 64)
	if err != nil {
		return r, errors.ErrInvalidCoordinateFormat.WithStack()
	}

	// validate latitude value
	if lat < -90 || lat > +90 {
		return r, errors.ErrInvalidCoordinateValue.WithStack()
	}

	// validate longitude value
	if lng < -180 || lat > +180 {
		return r, errors.ErrInvalidCoordinateValue.WithStack()
	}

	return types.Coordinate{
		Lat: lat,
		Lng: lng,
	}, nil
}
