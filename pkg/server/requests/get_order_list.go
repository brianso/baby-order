package requests

import (
	"baby-order/pkg/common/errors"
	"baby-order/pkg/common/types"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
)

type GetOrderListRequest struct {
	types.PageQuery
}

type GetOrderListValidator struct {
	GetOrderListRequest
}

func (s *GetOrderListValidator) Bind(c *gin.Context) error {
	b := binding.Default(c.Request.Method, c.ContentType())
	err := c.ShouldBindWith(s, b)
	if err != nil {
		return errors.ErrInvalidRequest.WithStack()
	}

	if s.Page == 0 {
		return errors.ErrInvalidPageValue.WithStack()
	}

	return nil
}
