package server

import (
	distanceclient "baby-order/pkg/service/distance/client"
	orderImpl "baby-order/pkg/service/order/impl"

	"github.com/gin-gonic/gin"
)

func NewServer() (*gin.Engine, error) {
	distanceService, err := distanceclient.NewDistanceClient()
	if err != nil {
		return nil, err
	}
	orderService := orderImpl.NewService(distanceService)

	engine := gin.Default()
	SetupRoutes(engine, orderService)

	return engine, nil
}
