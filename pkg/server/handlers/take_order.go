package handlers

import (
	"baby-order/pkg/server/requests"
	"baby-order/pkg/server/responses"
	"baby-order/pkg/service/order"
	"net/http"

	"github.com/gin-gonic/gin"
)

func createTakeOrderHandler(srv order.Service) gin.HandlerFunc {
	return func(c *gin.Context) {
		// Request
		var req requests.TakeOrderRequestValidator
		if err := req.Bind(c); err != nil {
			errorHandler(c, err)
			return
		}

		// Handle
		_, err := srv.ModifyOrderStatus(req.ID, req.Status)
		if err != nil {
			errorHandler(c, err)
			return
		}

		// Response
		c.JSON(http.StatusOK, responses.ToTakeOrderResponse())
	}
}
