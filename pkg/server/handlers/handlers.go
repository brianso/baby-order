package handlers

import (
	"baby-order/pkg/service/order"
	"fmt"

	"github.com/gin-gonic/gin"
)

type OrderHandlers struct {
	PlaceOrder   gin.HandlerFunc
	TakeOrder    gin.HandlerFunc
	GetOrderList gin.HandlerFunc
}

func NewOrderHandlers(srv order.Service) OrderHandlers {
	return OrderHandlers{
		PlaceOrder:   createPlaceOrderHandler(srv),
		TakeOrder:    createTakeOrderHandler(srv),
		GetOrderList: createGetOrderListHandler(srv),
	}
}

// return a readable string of the request body
func printRequestBody(c *gin.Context) string {
	if c.ContentType() == "application/json" {
		json := map[string]interface{}{}
		err := c.BindJSON(json)
		if err == nil {
			return fmt.Sprintf("%v", json)
		}
	}
	if c.ContentType() == "application/x-www-form-urlencoded" {
		err := c.Request.ParseForm()
		if err == nil {
			return c.Request.Form.Encode()
		}
	}
	return "<unable to decode>"
}
