package handlers

import (
	babyerrors "baby-order/pkg/common/errors"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

type ErrorResponse struct {
	Error string `json:"error"`
}

// handle BabyOrderError gracefully
// response 500 internal error if a non BabyOrderError caught
func errorHandler(c *gin.Context, err error) {
	switch err := errors.Cause(err).(type) {
	case babyerrors.BabyOrderError:
		c.JSON(err.Status, ErrorResponse{
			Error: err.Message,
		})
	default:
		c.JSON(http.StatusInternalServerError, ErrorResponse{
			Error: "Internal Server Error",
		})
		logrus.Errorf(
			"Request Internal Server Error: \n"+
				"Path: %s %s\n"+
				"Request: %s\n"+
				"Error: %+v\n",
			c.Request.Method, c.Request.URL,
			printRequestBody(c),
			err,
		)
	}
}
