package handlers

import (
	"baby-order/pkg/server/requests"
	"baby-order/pkg/server/responses"
	"baby-order/pkg/service/order"
	"net/http"

	"github.com/gin-gonic/gin"
)

func createPlaceOrderHandler(srv order.Service) gin.HandlerFunc {
	return func(c *gin.Context) {
		// Request
		var req requests.PlaceOrderRequestValidator
		if err := req.Bind(c); err != nil {
			errorHandler(c, err)
			return
		}

		// Handle
		placedOrder, err := srv.PlaceOrder(req.Origin, req.Destination)
		if err != nil {
			errorHandler(c, err)
			return
		}

		// Response
		c.JSON(http.StatusOK, responses.ToOrderResponse(placedOrder))
	}
}
