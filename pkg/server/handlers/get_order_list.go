package handlers

import (
	"baby-order/pkg/server/requests"
	"baby-order/pkg/server/responses"
	"baby-order/pkg/service/order"
	"net/http"

	"github.com/gin-gonic/gin"
)

func createGetOrderListHandler(srv order.Service) gin.HandlerFunc {
	return func(c *gin.Context) {
		// Request
		var req requests.GetOrderListValidator
		if err := req.Bind(c); err != nil {
			errorHandler(c, err)
			return
		}

		// Handle
		orders, err := srv.GetOrderList(req.PageQuery)
		if err != nil {
			errorHandler(c, err)
			return
		}

		// Response
		c.JSON(http.StatusOK, responses.ToOrderListResponse(orders))
	}
}
