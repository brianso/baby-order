package server

import (
	"baby-order/pkg/server/handlers"
	"baby-order/pkg/service/order"

	"github.com/gin-gonic/gin"
)

func SetupRoutes(engine *gin.Engine, srv order.Service) {
	engine.GET("ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})

	orderHandlers := handlers.NewOrderHandlers(srv)

	engine.POST("/orders", orderHandlers.PlaceOrder)
	engine.PATCH("/orders/:id", orderHandlers.TakeOrder)
	engine.GET("/orders", orderHandlers.GetOrderList)
}
