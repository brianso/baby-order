package responses

import "baby-order/pkg/db/model"

type OrderResponse struct {
	ID       uint64 `json:"id"`
	Distance uint   `json:"distance"`
	Status   string `json:"status"`
}

func ToOrderResponse(order *model.Order) OrderResponse {
	return OrderResponse{
		ID:       order.ID,
		Distance: order.Distance,
		Status:   order.Status,
	}
}
