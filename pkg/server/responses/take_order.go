package responses

const ResponseStatusSuccess = "SUCCESS"

type TakeOrderResponse struct {
	Status string `json:"status"`
}

func ToTakeOrderResponse() TakeOrderResponse {
	return TakeOrderResponse{
		Status: ResponseStatusSuccess,
	}
}
