package responses

import "baby-order/pkg/db/model"

func ToOrderListResponse(orders []*model.Order) []OrderResponse {
	result := make([]OrderResponse, len(orders))
	for i, o := range orders {
		result[i] = ToOrderResponse(o)
	}
	return result
}
