package db

import (
	"baby-order/pkg/db/model"
	"testing"

	"github.com/jinzhu/gorm"
)

func Test_DBConnection(t *testing.T) {
	var order model.Order
	err := DB().First(&order).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		t.Errorf("Query error: %v", err)
	}
}
