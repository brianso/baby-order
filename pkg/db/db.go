package db

import (
	"baby-order/pkg/common/config"
	"fmt"
	"sync"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	log "github.com/sirupsen/logrus"
)

var db *gorm.DB
var once sync.Once

// return the connected gorm.DB singleton
func DB() *gorm.DB {
	once.Do(func() {
		connectionString := fmt.Sprintf(
			"%s:%s@(%s:%s)/%s?parseTime=true",
			config.GetDBUser(),
			config.GetDBPassword(),
			config.GetDBHost(),
			config.GetDBPort(),
			config.GetDBName(),
		)

		var err error
		db, err = gorm.Open("mysql", connectionString)
		if err != nil {
			log.Fatalln("init DB error: ", err)
		}
	})
	return db
}
