package model

const (
	OrderStatusUnassigned = "UNASSIGNED"
	OrderStatusTaken      = "TAKEN"
)

type Order struct {
	ID             uint64 `gorm:"primary_key" json:"id"`
	OriginLat      float64
	OriginLng      float64
	DestinationLat float64
	DestinationLng float64
	Status         string

	// in meter
	Distance uint
}
