package db

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// inject the passed db to the global singleton
func InjectMockDB(injectDB *gorm.DB) *gorm.DB {
	once.Do(func() {})
	db = injectDB
	return db
}
