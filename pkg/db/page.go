package db

import (
	"baby-order/pkg/common/types"

	"github.com/jinzhu/gorm"
)

// prepend page configuration to the gorm.DB object
//
// Example: PageDB(types.PageQuery{ Page: 2, Limit: 10 }).Find(&model)
//  		-> SELECT * FROM model OFFSET 10 LIMIT 10
func PageDB(query types.PageQuery) *gorm.DB {
	return DB().Offset((query.Page - 1) * query.Limit).Limit(query.Limit)
}
