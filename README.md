# Baby Order

A simple go micro-service demo. [live demo](https://babyorder.brianso.space)

## Config the Server
```bash
# Config your environment
# Set GOOGLE_MAPS_API_KEY=<YOUR OWN GOOGLE MAPS API KEY>
cp .env.example .env
vi .env
```

## Start the Server
Prerequisite: `Docker` and `docker-compose` installed.

```bash
./start.sh
```

## Development
With docker:
```bash
# setup
./start.sh

# restart app
docker-compose up --build app

# run migration
docker-compose up --build migrations
```

Without docker:
```bash
# Install migrate
https://github.com/golang-migrate/migrate/tree/master/cmd/migrate

# run migrations
make db-up

# run
make run
```

## Project Structure

Design pattern:
1. Separated implementation of Server (http) and Service
    1. Server api changes does not require changes to Service
    2. assume Service called with valid params only
2. Service defined as interface
    1. service can be implemented (order.Service)
    2. service can be implemented as a remote client (distance.Service)
    3. service can be mocked (pkg/service/distance/mock) 
3. Meaningful full function rather than short function name with comments

```
baby-order/
 |- entrypoints/             # entry point of the micro service
 |- integration_tests/       # jest tests against the http api
 |- migrations/              # database migration files
 |- pkg/                     # main source codes
     |- common/
     |   |- config/          # utils for getting config
     |   |- errors/          # error definitions
     |   |- types/           # common types
     |
     |- db/
     |   |- model/           # database model definitions
     |
     |- server/
     |   |- handlers/        # http handlers
     |   |- requests/        # request definitions and validators
     |   |- responses/       # response definitions and convertos
     |   |- routes.go        # http routes definitions
     |   |- server.go        # http server
     |
     |- service/
         |- distance/
             |- client/      # distance service client implementation
             |- service.go   # service definition
         |- order/
             |- impl/        # order service implementation
             |- service.go   # service definition
```