#!make

# load .env file
include .env
export $(shell sed 's/=.*//' .env)

GO = go
BUILD = $(GO) build

BUILD_DIR ?= ./build

MIGRATE = migrate
DB_HOST ?= 127.0.0.1
DB_PORT ?= 3306
DB_NAME ?= babyorder
DB_USER ?= babyorder
DB_PASSWORD ?= xypGs9sp88UzyAw4

all: babyorder

run: run-babyorder

babyorder:
	$(BUILD) -o $(BUILD_DIR)/babyorder ./entrypoints/order

run-babyorder: babyorder
	$(BUILD_DIR)/babyorder


## DB
db-up:
	${MIGRATE} -database "mysql://$(DB_USER):$(DB_PASSWORD)@($(DB_HOST):$(DB_PORT))/$(DB_NAME)?parseTime=true" -path ./migrations up

db-down:
	${MIGRATE} -database "mysql://$(DB_USER):$(DB_PASSWORD)@($(DB_HOST):$(DB_PORT))/$(DB_NAME)?parseTime=true" -path ./migrations down 1
